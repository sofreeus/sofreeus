This repository is where the business of SFS is managed, where we plan and produce learning events.

Links that are useful to SFS Captains:
- [Planning Schedule](schedule.md)
- [Issue Board](https://gitlab.com/sofreeus/sofreeus/-/boards)
- [SoFreeUs Meetup](https://www.meetup.com/sofreeus/)
- [SFS BizAdmins on Mattermost](http://mm.sofree.us/sfs-team/channels/sfs-bizadmins)
- [Zimbra Collaboration Server](https://blue.sofree.us:8443/)
- [www.SoFree.Us](www.sofree.us) ([Source](https://gitlab.com/sofreeus.gitlab.io))
- [Nextcloud](https://nextcloud.sofree.us)

---

- [Facebook Group](https://www.facebook.com/groups/sofreeus/)
- [Facebook Page](https://www.facebook.com/softwarefreedomschool/)