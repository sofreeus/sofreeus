# Moodle Evaluation and Recommendation

### What is Moodle?

Moodle is a learning management system (LMS) that improves communications between an organization and its students.  It is designed to be a customizable portal for teachers and students alike to manage course information, scheduling, and other communications.

### Current Tooling/Stack

- Course Materials
  - GitLab
- Course Metadata
  - Meetup
- Course Delivery
  - BigBlueButton
  - Zoom
- Communications
  - Mailing lists
  - Mattermost
  - Meetup
- Payments
  - Cash jar
  - Venmo
  - PayPal
- Feedback
  - Google Forms

### Evaluation

Likely Valuable:
- BigBlueButton integration.  Outstanding concern: There are [published limitations](https://docs.moodle.org/401/en/BigBlueButton), but I believe these restrictions are only for Moodle instances hosted on moodle.com.
- [Survey](https://docs.moodle.org/401/en/Survey_activity) could replace Google Forms for the end-of-class survey.
- Students have a consolidated view of their historical activities with SFS.
- [Payment gateway](https://moodle.org/plugins/browse.php?list=category&id=80) plugins - Getting past any security concerns, there's really only one well-known provider here (Stripe).  But it could be extremely valuable nonetheless.  PayPal used to be an option, but it has been removed.

Potentially Valuable:
- Communications - Notifications seem more course-related (e.g. teacher comments, assignments, forum activity, etc.) instead of organization-related (e.g. new class posting, org-wide announcement).  That could potentially be mitigated by posting new class links in the forums and having a specific form for job postings, for example.
- Scheduling/calendar - All classes added to Moodle appear in the calendar for the organization.
  - This _might_ help us build a schedule
  - Would be nicer than Meetup's view for students
- [Insights/analytics/reporting](https://docs.moodle.org/401/en/Site-wide_reports) - More investigation needed to determine if we could get what we want from this.
- Forum and Wiki - These would likely take a lot of effort to keep active.  This seems like something we'd be excited about at first, but it would fade into the background.  We would have to come up with a way for our community activity to achieve critical mass.
- "Choice" plugin could be used for quick, non-verbal status reports.
- Quizzes 
  - Would we be able to successfully maintain practice exams for other companies' certification exams?  It would be a lot of work to keep those up to date.  
  - Having practice exams for Linux Camp or DevOps Camp could be more practical as knowledge-check devices.
- There's a mobile app for Moodle.  
  - Some people might like this, but I don't think the bulk of our materials/interaction is great on mobile, even with a nice app.
  - But the use of push notifications and scheduling could be really handy here.
- Moodle is good for tracking homework assignments, which could be good for study groups, camps, or other multi-day classes.

Redundant:
- Chat is available on a per-course basis.  This would not be able to replace Mattermost for the more holistic communications.  Additionally, the features users are familiar with (image uploading, threading, Markdown formatting) are not available.
- Content hosting ... do we replace GitLab?  I like that GitLab allows teachers to put course materials fully under the SFS umbrella (e.g. https://gitlab.com/sofreeus/introtodevops) or maintain them in their own domain (e.g. https://gitlab.com/aayore/introtodevops).  Though this can also be redundant (see examples) if not managed well.

Unlikely Valuable:
- Course management
  - This is a major component of Moodle, so it seems silly to just make it a link to GitLab
  - Metadata (think: Meetup description) must be added manually in the GUI (though this isn't really worse than Meetup) or via custom CSV
  - Course materials seem to require manual creation/editing via the Moodle GUI
- Assignments and grading - I don't see much utility at all for this.  Grading doesn't seem to apply to either of the course types we offer (professional-level or casual).
- Though Moodle can be used for one-and-done events, the software really specializes in recurring/serial classes (daily or weekly).

### Recommendation

Moodle isn't an obvious choice to implement.  It's primary specialties - serial courses, materials, and assignment management - are low-value to SFS.  The clearly valuable features - payments and BigBlueButton integration - would be nice to have, but alone, they wouldn't be critical path to SFS class operations.

The only tool in our stack that Moodle is likely to eliminate is Meetup.  Because Meetup is a hosted service and Moodle would not be, the maintenance load on the SysOps would increase.  (Though it's worth noting there are "Moodle Certified Service Providers" providing for-pay hosted instances.)  We don't like Meetup, but it gives us a few things:
1. Discoverability - Without internal expertise in marketing or SEO, we're able to reach students in our area with similar or overlapping interests with very little effort.  This would require a lot of work by the SFS inner circles to offset.
2. Accessibility - Most of our student base is familiar with Meetup to some extent.  Very few are likely to be familiar with Moodle (at least at first), so there will be a learning curve.  Without determined efforts by students, teachers, and admins, many of the features of Moodle may go unexplored and/or unused.
3. Existing audience - There will be attrition trying to get our Meetup members to sign up for our Moodle instance.

Despite these drawbacks, there are a lot of items in the "potentially valuable" list.  Chiefly, the communications and scheduling could be very useful if we invest the time to manage them well.  The reporting features could simplify (or un-overwhelm?) the challenging tasks we face with annual learner surveys and other business insights.  More investigation into this feature set, in particular, could be valuable.  The sum of all these features could provide a lot of value to SFS, but they would also require a lot of work.  If we could recruit someone to the inner circle as a "Marketing and Engagement Manager," I think there's a clear role to manage a lot of the features within Moodle.

All in all, Moodle has potential.  But the amount of work required to leverage its potential value is more than we have at our disposal right now.  Along the lines of the recruit mentioned above, we could learn a lot with a small investment if we could have an "intern" or interested party take the reins with Moodle and pilot it for production operations in parallel with our current processes.