# The Gathering

## What is the Gathering?

The Gathering is our monthly meeting. It is also called Software Freedom First Saturday. It occurs on the first Saturday of each month.

For SFS303, it occurs from 9AM to noon, usually at SFS HQ, which is also called The Comfy Room, or Casa de Willson.

### What do Gatherings consist of?

The Gathering's primary purpose is to build our Community, our mutual aid and admiration society. We come together to strengthen the bond of friendship. We smile and wave at one another, we greet one another by name, maybe we fist-bump, high-five, shake hands, or hug. We welcome new friends.

Because we are a learning community, we do 'show and tell' sessions.

### Two Featured Show-and-Tell Sessions

- Each session needs topic and teacher
- The first session should be "Tuned for 22", about 22 minutes in length
- Then short break
- Then play gathering track again to tell everyone to quiet down
- The second session, like the first, can be talk and demo, or participatory. Unlike the first, it can be a deep dive.
- The second session should be about an hour long. It might follow [The Time Pie](https://gitlab.com/sofreeus/sfs-method/-/blob/master/units/the-time-pie.md) format.

Presenters can present anything as long as it aligns with free software values. This can be a discussion on the newest firmware for toaster ovens, as long as it meets [The four essential freedoms](https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms). It can be a demonstration of a new knitting technique, as long as that technique is free-libre, not proprietary.

**Plan** Gathering events like other events, but ensure that there are two in who and what.

**Post** Gathering events like other events.

### How to MC a Gathering

This guide is for the person who will be the MC, the master or mistress of ceremonies, for a Gathering. The MC may or may not also be the Planner.

If the MC is not the Planner, MC and Planner have a mutual relationship where each acts as support and overwatch for the other, ensuring that tasks are completed effectively, stepping in if necessary, and providing guidance and oversight.

The Planner is responsible for Who, What, and Where. When, for a Gathering, is always the first Saturday of the month. For SFS303, it happens from 9AM to noon Denver time.

The MC is responsible for Place and Pace.

Friday night:

  1. Clean and set the room.
  * Make sure BBB is working. Contact @rcalv002 in Mattermost, if it's not.
  * If BBB can't be restored quickly, switch to Jitsi and update the posting.

Saturday:

  1. 8:30am - Open doors. Put the lighted sign in the front picture window. Join BBB.
  * 9AM - Share video in BBB - Frank Turner's Gathering
  * Read the Rap
  * Call for intros all around: Who needs work? Who needs workers?
  * Introduce a featured session and it's presenter
  * Be a Learner: Listen actively, participate if possible, add value if possible, and encourage other learners to do likewise.
  * Close the session
  * if there are more sessions in queue
    - Declare a break with duration
    - Put up a big timer with a loud noise at the end, if you can
    - Go to "Intro a session"
  * When there are no more planned sessions, open the mic to anyone who may want to show something or talk about something
  * Ask for pay
  * Go outside and casterboard or hang out and chat
  * Clean up
